import tensorflow as tf
import utils.data_utils as du
import nltk


class VanillaModel(object):
    def __init__(self,
                 session,
                 vocabulary_size,
                 learning_rate=0.0003,
                 max_gradient_norm=5.0,
                 num_of_units=256,
                 max_iterations=25,
                 embedding_init_matrix=None):

        self.vocabulary_size = vocabulary_size
        self.learning_rate = learning_rate
        self.max_gradient_norm = max_gradient_norm
        self.max_iterations = max_iterations

        self.session = session

        self.encoder_inputs = tf.placeholder(shape=(None, None), dtype=tf.int32, name='inputs')
        self.encoder_inputs_length = tf.placeholder(shape=(None,), dtype=tf.int32, name='inputs_length')
        self.targets = tf.placeholder(shape=(None, None), dtype=tf.int32, name='targets')
        self.targets_length = tf.placeholder(shape=(None,), dtype=tf.int32, name='targets_length')

        self.actual_batch_size = tf.shape(self.encoder_inputs)[0]
        self.output_max_length = tf.shape(self.targets)[1]

        self.start_tokens = tf.tile([du.GO_TOKEN_ID], [self.actual_batch_size])
        self.end_tokens = tf.tile([du.EOS_TOKEN_ID], [self.actual_batch_size])
        self.decoder_inputs = tf.concat([tf.expand_dims(self.start_tokens, 1), self.targets], 1)
        self.decoder_inputs_length = self.targets_length + tf.ones_like(self.targets_length)

        self.total_valid_bleu_score = 0.0
        self.total_valid_number = 0
        self.best_validation_example = (0.0, None, None, None)

        with tf.variable_scope("EMBEDDING"):
            if embedding_init_matrix is None:
                self.embedding = tf.get_variable("matrix", shape=(self.vocabulary_size, num_of_units),
                                                 initializer=tf.truncated_normal_initializer(),
                                                 dtype=tf.float32, trainable=True)
            else:
                self.embedding = tf.get_variable("matrix",
                                                 shape=(len(embedding_init_matrix), len(embedding_init_matrix[0])),
                                                 initializer=tf.constant_initializer(embedding_init_matrix),
                                                 dtype=tf.float32, trainable=True)
            self.emb_encoder_inputs = tf.nn.embedding_lookup(self.embedding, self.encoder_inputs, name="encoder_lookup")
            self.emb_decoder_inputs = tf.nn.embedding_lookup(self.embedding, self.decoder_inputs, name="decoder_lookup")

        with tf.variable_scope("ENCODER"):
            self.encoder_cell_fw_lvl0 = tf.contrib.rnn.GRUCell(num_units=num_of_units / 2)
            self.encoder_cell_bw_lvl0 = tf.contrib.rnn.GRUCell(num_units=num_of_units / 2)
            self.encoder_output_lvl0, self.encoder_final_state_lvl0 = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=self.encoder_cell_fw_lvl0,
                cell_bw=self.encoder_cell_bw_lvl0,
                inputs=self.emb_encoder_inputs,
                sequence_length=self.encoder_inputs_length,
                time_major=False, dtype=tf.float32)
            self.encoder_layer_0_output = tf.concat(self.encoder_output_lvl0, 2)

            self.encoder_cell = tf.contrib.rnn.MultiRNNCell(
                cells=[tf.contrib.rnn.GRUCell(num_units=num_of_units) for _ in range(3)], state_is_tuple=True)
            self.encoder_ouputs_lvl1, self.encoder_final_state_lvl1 = tf.nn.dynamic_rnn(cell=self.encoder_cell,
                                                                                        inputs=self.emb_encoder_inputs,
                                                                                        sequence_length=self.encoder_inputs_length,
                                                                                        time_major=False,
                                                                                        dtype=tf.float32)
            self.encoder_final_state = (tf.concat(self.encoder_final_state_lvl0, 1),) + self.encoder_final_state_lvl1

            self.attention_mechanism = tf.contrib.seq2seq.LuongAttention(
                num_units=num_of_units, memory=self.encoder_ouputs_lvl1,
                memory_sequence_length=self.encoder_inputs_length)

        with tf.variable_scope("DECODER"):
            self.decoder_cells = [tf.contrib.rnn.GRUCell(num_units=num_of_units) for _ in range(4)]

            self.decoder_cell = tf.contrib.rnn.MultiRNNCell(
                cells=self.decoder_cells, state_is_tuple=True)

            self.attn_cell = tf.contrib.seq2seq.AttentionWrapper(
                self.decoder_cell, self.attention_mechanism, attention_layer_size=num_of_units)

            self.initial_state = (self.attn_cell.zero_state(self.actual_batch_size, tf.float32)).clone(
                cell_state=self.encoder_final_state)

            self.out_cell = tf.contrib.rnn.OutputProjectionWrapper(
                self.attn_cell, self.vocabulary_size, reuse=None
            )

            self.train_helper = tf.contrib.seq2seq.TrainingHelper(self.emb_decoder_inputs, self.targets_length)

            self.train_decoder = tf.contrib.seq2seq.BasicDecoder(
                cell=self.out_cell, helper=self.train_helper,
                initial_state=self.initial_state)
            # initial_state=encoder_final_state)

            self.train_outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(
                decoder=self.train_decoder, output_time_major=False,
                impute_finished=True, maximum_iterations=tf.reduce_max(self.decoder_inputs_length)
            )

            self.train_decoder_outputs = self.train_outputs.rnn_output

            self.pred_helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
                embedding=self.embedding,
                start_tokens=self.start_tokens,
                end_token=du.EOS_TOKEN_ID)

            self.pred_decoder = tf.contrib.seq2seq.BasicDecoder(
                cell=self.out_cell, helper=self.pred_helper,
                initial_state=self.initial_state)

            self.pred_outputs, _, _ = tf.contrib.seq2seq.dynamic_decode(
                decoder=self.pred_decoder, output_time_major=False,
                impute_finished=True, maximum_iterations=self.max_iterations
            )

            self.pred_decoder_outputs = self.pred_outputs.rnn_output
            self.pred_argmax = tf.argmax(self.pred_decoder_outputs, axis=2)

        self.weights = tf.to_float(tf.not_equal(self.decoder_inputs[:, :-1], du.EOS_TOKEN_ID))

        self.crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.train_decoder_outputs,
                                                                       labels=self.targets)

        self.loss = tf.reduce_sum(self.crossent * self.weights) / tf.cast(self.actual_batch_size, tf.float32)

        # Calculate and clip gradients
        self.params = tf.trainable_variables()
        self.gradients = tf.gradients(self.loss, self.params)
        self.clipped_gradients, _ = tf.clip_by_global_norm(
            self.gradients, self.max_gradient_norm)

        # Optimization
        self.optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.train_op = self.optimizer.apply_gradients(
            zip(self.clipped_gradients, self.params))

        self.saver = tf.train.Saver()
        self.init_variables = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        session.run(self.init_variables)

    def load(self, path):
        try:
            self.saver.restore(self.session, path)
        except Exception as e:
            # print(e)
            print("Cannot be read")

    def save(self, path):
        save_path = self.saver.save(self.session, path)
        print("Saved at " + save_path)

    def train_step(self, inputs, inputs_lengths, targets, targets_lengths):
        feed_dict = {
            self.encoder_inputs: inputs,
            self.encoder_inputs_length: inputs_lengths,
            self.targets: targets,
            self.targets_length: targets_lengths
        }
        loss, _ = self.session.run([self.loss, self.train_op], feed_dict=feed_dict)
        return loss

    def train_step_with_accuracy(self, batch):
        pass

    def start_validation(self):
        self.total_valid_bleu_score = 0.0
        self.total_valid_number = 0
        self.best_validation_example = (0.0, None, None, None)

    def validate_step(self, inputs, inputs_lengths, references, convert_fun):
        feed_dict = {
            self.encoder_inputs: inputs,
            self.encoder_inputs_length: inputs_lengths,
        }
        prediction_argmax = self.session.run([self.pred_argmax], feed_dict=feed_dict)[0].tolist()
        self.total_valid_number += len(references)
        local_best_bleu_score = 0.0
        local_best_example = (local_best_bleu_score, [], [], [])
        for indx, reference in enumerate(references):
            ref_words = convert_fun(reference)
            hypothesis = convert_fun(prediction_argmax[indx])
            BLEUscore = nltk.translate.bleu_score.sentence_bleu([ref_words], hypothesis)
            if BLEUscore > local_best_bleu_score:
                local_best_bleu_score = BLEUscore
                local_best_example = (BLEUscore, convert_fun(inputs[indx]), ref_words, hypothesis)
            if BLEUscore > self.best_validation_example[0]:
                self.best_validation_example = (BLEUscore, convert_fun(inputs[indx]), ref_words, hypothesis)
        return local_best_example

    def stop_validation(self):
        final_score = self.total_valid_bleu_score / self.total_valid_number
        self.total_valid_bleu_score = 0.0
        self.total_valid_number = 0
        return final_score, self.best_validation_example