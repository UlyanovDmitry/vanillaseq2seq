import numpy as np
import random

GO_TOKEN = "<GO>"
GO_TOKEN_ID = 0
EOS_TOKEN = "<EOS>"
EOS_TOKEN_ID = 1
PAD_TOKEN = "<PAD>"
PAD_TOKEN_ID = 2
UNK_TOKEN = "<UNK>"
UNK_TOKEN_ID = 3

TRAIN_DATASET = None
VALID_DATASET = None
VOCABULARY = None


def create_vocab(path_to_data):
    from gensim.models import Word2Vec
    model = Word2Vec.load(path_to_data + "/word2vec")
    with open(path_to_data + "/vocab.txt", "w") as file:
        file.write("{}-{}\n".format(GO_TOKEN_ID, GO_TOKEN))
        file.write("{}-{}\n".format(EOS_TOKEN_ID, EOS_TOKEN))
        file.write("{}-{}\n".format(PAD_TOKEN_ID, PAD_TOKEN))
        file.write("{}-{}\n".format(UNK_TOKEN_ID, UNK_TOKEN))
        for index, word in enumerate(model.wv.vocab.keys()):
            file.write("{}-{}\n".format(index + 4, word))


def get_vocab(path_to_data):
    w2id = {}
    id2w = {}
    with open(path_to_data + "/vocab.txt") as file:
        lines = file.readlines()
        for line in lines:
            params = line.split('-')
            _id = int(params[0])
            _word = params[1][:-1]
            w2id[_word] = _id
            id2w[_id] = _word
    return w2id, id2w


def get_matrix(path_to_data):
    from gensim.models import Word2Vec
    model = Word2Vec.load(path_to_data + "/word2vec")
    length = model.wv.vector_size
    matrix = [np.random.uniform(-1, 1, length).tolist(),
              np.random.uniform(-1, 1, length).tolist(),
              np.random.uniform(-1, 1, length).tolist(),
              np.random.uniform(-1, 1, length).tolist()]
    _, id2w = get_vocab(path_to_data)
    for i in range(4, len(id2w.keys())):
        word = id2w[i]
        matrix.append(model.wv[word].tolist())
    return matrix


def normilize(s):
    import re
    s = re.sub(r'[^a-z #]', '', s.lower())
    s = re.sub(r'#+', '#', s.lower())
    s = re.sub(r'#', ' # ', s.lower())
    return s.split()


def tokenize(sentence):
    if VOCABULARY is not None:
        _norm = normilize(sentence)
        _res = []
        w2id = VOCABULARY[0]
        for _w in _norm:
            if _w in w2id.keys():
                _res.append(w2id[_w])
            else:
                _res.append(w2id[UNK_TOKEN])
        return _res


def detokenize(tokens):
    if VOCABULARY is not None:
        _res = []
        id2w = VOCABULARY[1]
        for token in tokens:
            _res.append(id2w[token])
        return _res


def init(path_to_data, train_rows_count = -1, valid_row_count = -1):
    global VOCABULARY
    global TRAIN_DATASET
    global VALID_DATASET
    VOCABULARY = get_vocab(path_to_data)
    TRAIN_DATASET = []
    train_articles_norm = []
    train_titles_norm = []
    with open(path_to_data + "/train_article.txt") as article_file:
        if train_rows_count > 0:
            lines = article_file.readlines()[:train_rows_count]
        else:
            lines = article_file.readlines()
        for line in lines:
            train_articles_norm.append(tokenize(line))
    with open(path_to_data + "/train_title.txt") as title_file:
        if train_rows_count > 0:
            lines = title_file.readlines()[:train_rows_count]
        else:
            lines = title_file.readlines()
        for line in lines:
            train_titles_norm.append(tokenize(line))
    TRAIN_DATASET = list(zip(train_articles_norm, train_titles_norm))

    VALID_DATASET = []
    valid_articles_norm = []
    valid_titles_norm = []
    with open(path_to_data + "/valid_article.txt") as article_file:
        if valid_row_count > 0:
            lines = article_file.readlines()[:valid_row_count]
        else:
            lines = article_file.readlines()
        for line in lines:
            valid_articles_norm.append(tokenize(line))
    with open(path_to_data + "/valid_title.txt") as title_file:
        if valid_row_count > 0:
            lines = title_file.readlines()[:valid_row_count]
        else:
            lines = title_file.readlines()
        for line in lines:
            valid_titles_norm.append(tokenize(line))
    VALID_DATASET = list(zip(valid_articles_norm, valid_titles_norm))


def _create_batch(data):
    _max_artile_length = len(max(data, key=lambda x: len(x[0]))[0])
    _max_title_length = len(max(data, key=lambda x: len(x[1]))[1])
    _artiles = np.full(shape=[len(data), _max_artile_length], fill_value=UNK_TOKEN_ID, dtype=np.int32)
    _artile_lengths = np.zeros(shape=[len(data)], dtype=np.int32)
    _titles = np.full(shape=[len(data), _max_title_length], fill_value=UNK_TOKEN_ID, dtype=np.int32)
    _title_lengths = np.zeros(shape=[len(data)], dtype=np.int32)
    for index, val in enumerate(data):
        article = val[0]
        title = val[1]
        _artile_lengths[index] = len(article)
        _title_lengths[index] = len(title)
        for j, v in enumerate(article):
            _artiles[index][j] = v
        for j, v in enumerate(title):
            _titles[index][j] = v
    return (
        _artiles,
        _artile_lengths,
        _titles,
        _title_lengths
    )


def get_batch(batch_size=32):
    if TRAIN_DATASET is not None:
        data = random.sample(TRAIN_DATASET, batch_size)
        return _create_batch(data)


_current_valid_index = 0


def get_next_valid_rows(batch_size=32):
    global _current_valid_index
    if _current_valid_index == len(VALID_DATASET):
        _current_valid_index = 0
    data = VALID_DATASET[_current_valid_index: _current_valid_index+batch_size]
    _current_valid_index += len(data)
    if len(data) < batch_size:
        _current_valid_index = 0
    return _create_batch(data)
